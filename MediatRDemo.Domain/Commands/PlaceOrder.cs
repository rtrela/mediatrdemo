﻿using System;
using MediatR;

namespace MediatRDemo.Domain.Commands
{
    public class PlaceOrder : IRequest
    {
        public Guid ProductId { get; set; }
        public decimal Quantity { get; set; }
        public Guid BuyerId { get; set; }
    }
}