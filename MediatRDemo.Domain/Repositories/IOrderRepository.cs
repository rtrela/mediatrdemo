﻿namespace MediatRDemo.Domain.Repositories
{
    public interface IOrderRepository
    {
        void Add(Entities.Order order);
    }
}