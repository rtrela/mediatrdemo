﻿using System;
using MediatR;

namespace MediatRDemo.Domain.Events
{
    public class OrderPlaced : IRequest<Unit>, INotification
    {
        public Guid OrderId { get; set; }
    }
}