﻿using System;

namespace MediatRDemo.Domain.Entities
{
    public class Order
    {
        public Order(
            Guid productId,
            decimal quantity,
            Guid buyerId)
        {
            ProductId = productId;
            Quantity = quantity;
            BuyerId = buyerId;
        }
        
        public Guid ProductId { get; }
        public decimal Quantity { get; }
        public Guid BuyerId { get; }
    }
}