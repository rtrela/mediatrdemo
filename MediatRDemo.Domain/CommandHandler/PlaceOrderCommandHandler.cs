﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using MediatR;
using MediatRDemo.Domain.Commands;
using MediatRDemo.Domain.Entities;
using MediatRDemo.Domain.Repositories;

namespace MediatRDemo.Domain.CommandHandler
{
    public class PlaceOrderCommandHandler : RequestHandler<PlaceOrder>
    {
        private readonly IMediator _mediator;
        private readonly IOrderRepository _orderRepository;
        
        public PlaceOrderCommandHandler(
            IMediator mediator,
            IOrderRepository orderRepository)
        {
            _mediator = mediator;
            _orderRepository = orderRepository;
        }

        protected override void Handle(PlaceOrder request)
        {
            Debug.WriteLine("PlaceOrderCommandHandler.Handle called");
            
            _orderRepository.Add(new Order(request.ProductId, request.Quantity, request.BuyerId));
            
            _mediator.Publish(new Events.OrderPlaced
            {
                OrderId = Guid.NewGuid()
            });
            Debug.WriteLine("PlaceOrderCommandHandler.Handle completed");
        }
    }
}