﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using MediatR;
using MediatRDemo.Queries.Orders;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MediatRDemo.WebApp.Models;
using Microsoft.Extensions.Configuration.CommandLine;
using Commands = MediatRDemo.Domain.Commands;

namespace MediatRDemo.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMediator _mediator;

        public HomeController(ILogger<HomeController> logger,
            IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult PostOrder()
        {
            Debug.WriteLine("HomeController.PostOrder called");
            
            _mediator.Send(new Commands.PlaceOrder());
            
            Debug.WriteLine("HomeController.PostOrder completed");
            return Redirect(Url.Action("Index"));
        }

        public async Task<IActionResult> GetAllOrders()
        {
            Debug.WriteLine("HomeController.GetAllOrders called");
            
            var allOrders = await _mediator.Send(new GetAllOrdersRequest());
            
            Debug.WriteLine($"HomeController.GetAllOrders completed {JsonSerializer.Serialize(allOrders)}");
            return Ok(allOrders);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}