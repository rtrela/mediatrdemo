﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using MediatRDemo.Domain.Entities;
using MediatRDemo.Domain.Repositories;

namespace MediatRDemo.DataAccess.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        public void Add(Order order)
        {
            Debug.WriteLine("OrderRepository.Add called");
            Thread.Sleep(2000);
            Debug.WriteLine("OrderRepository.Add completed");
        }
    }
}