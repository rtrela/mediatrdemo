﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace MediatRDemo.Queries.Orders
{
    public class GetAllOrdersHandler : IRequestHandler<GetAllOrdersRequest, GetAllOrdersResponse>
    {
        public async Task<GetAllOrdersResponse> Handle(GetAllOrdersRequest request, CancellationToken cancellationToken)
        {
            Debug.WriteLine("GetAllOrdersHandler.Handle called");

            await Task.Delay(3000, cancellationToken);
            
            Debug.WriteLine("GetAllOrdersHandler.Handle completed");
            return new GetAllOrdersResponse
            {
                Orders = new []{ new Order { OrderId = Guid.NewGuid()} }
            };
        }
    }
}