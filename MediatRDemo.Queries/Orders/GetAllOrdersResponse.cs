﻿using System;
using System.Collections.Generic;

namespace MediatRDemo.Queries.Orders
{
    public class GetAllOrdersResponse
    {
        public Order[] Orders { get; set; }
    }

    public class Order
    {
        public Guid OrderId { get; set; }
    }
}