﻿using MediatR;

namespace MediatRDemo.Queries.Orders
{
    public class GetAllOrdersRequest : IRequest<GetAllOrdersResponse>
    {
    }
}