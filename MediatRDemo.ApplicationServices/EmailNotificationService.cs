﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Events = MediatRDemo.Domain.Events;

namespace MediatRDemo.ApplicationServices
{
    public class EmailNotificationService : INotificationHandler<Events.OrderPlaced>
    {
        public async Task Handle(Events.OrderPlaced request, CancellationToken cancellationToken)
        {
            Debug.WriteLine("EmailNotificationService.Handle called");
            await Task.Delay(4000, cancellationToken);
            Debug.WriteLine("EmailNotificationService.Handle completed");
        }
    }
}