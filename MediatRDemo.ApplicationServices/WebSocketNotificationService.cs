﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Events = MediatRDemo.Domain.Events;

namespace MediatRDemo.ApplicationServices
{
    public class WebSocketNotificationService : INotificationHandler<Events.OrderPlaced>
    {
        public async Task Handle(Events.OrderPlaced request, CancellationToken cancellationToken)
        {
            Debug.WriteLine("WebSocketNotificationService.Handle called");
            await Task.Delay(2000, cancellationToken);
            Debug.WriteLine("WebSocketNotificationService.Handle completed");
        }
    }
}